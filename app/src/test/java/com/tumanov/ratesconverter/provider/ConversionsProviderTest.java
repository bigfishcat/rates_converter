package com.tumanov.ratesconverter.provider;

import android.support.annotation.NonNull;

import com.tumanov.ratesconverter.model.Rate;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by andre on 26.03.2016.
 */
public class ConversionsProviderTest {

    private ConversionsProvider provider;

    @Before
    public void setUp() throws Exception {
        provider = new ConversionsProvider(new RatesProvider() {
            @NonNull
            @Override
            public List<Rate> getRates() {
                return Arrays.asList(new Rate("RUB", "USD", 2), new Rate("USD", "CAN", 3), new Rate("CAN", "GBP", 5));
            }
        });
        provider.refresh();
    }

    @Test
    public void testGetConversionRate() throws Exception {
        assertEquals(2., provider.getConversionRate("RUB", "USD"), .1);
        assertEquals(6., provider.getConversionRate("RUB", "CAN"), .1);
        assertEquals(30., provider.getConversionRate("RUB", "GBP"), .1);
        assertEquals(15., provider.getConversionRate("USD", "GBP"), .1);
        assertEquals(.033, provider.getConversionRate("GBP", "RUB"), .001);
    }

    @Test
    public void testGetAvailableCurrencies() throws Exception {
        assertEquals(new HashSet<>(Arrays.asList("RUB", "USD", "CAN", "GBP")), new HashSet<>(provider.getAvailableCurrencies()));
    }
}