package com.tumanov.ratesconverter.provider;

import com.tumanov.ratesconverter.model.Rate;
import org.junit.Before;
import org.junit.Test;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;


/**
 * Created by tumanov on 3/25/2016.
 */
public class RatesProviderTest {
    private RatesProvider provider;
    private final String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<gesmes:Envelope xmlns:gesmes=\"http://www.gesmes.org/xml/2002-08-01\" xmlns=\"http://www.ecb.int/vocabulary/2002-08-01/eurofxref\">\n" +
            "\t<gesmes:subject>Reference rates</gesmes:subject>\n" +
            "\t<gesmes:Sender>\n" +
            "\t\t<gesmes:name>European Central Bank</gesmes:name>\n" +
            "\t</gesmes:Sender>\n" +
            "\t<Cube>\n" +
            "\t\t<Cube time='2016-03-24'>\n" +
            "\t\t\t<Cube currency='USD' rate='1.1154'/>\n" +
            "\t\t\t<Cube currency='GBP' rate='0.78938'/>\n" +
            "\t\t\t<Cube currency='RUB' rate='77.7413'/>\n" +
            "\t\t</Cube>\n" +
            "\t</Cube>\n" +
            "</gesmes:Envelope>";

    private final List<Rate> PARSED_RATES = Arrays.asList(new Rate("EUR", "USD", 1.1154f), new Rate("EUR", "GBP", 0.78938f), new Rate("EUR", "RUB", 77.7413f));

    @Before
    public void setUp() {
        provider = new RatesProvider();
    }

    @Test
    public void testReadRates() throws IOException, XmlPullParserException {
        List<Rate> rates = provider.parseRates(new ByteArrayInputStream(XML.getBytes()));
        assertEquals(PARSED_RATES, rates);
    }

    @Test
    public void testGetRates() {
        List<Rate> rates = provider.getRates();
        assertNotNull(rates);
        assertFalse(rates.isEmpty());
        for (Rate rate : rates) {
            assertEquals(rate.from, "EUR");
            assertTrue(rate.rate > 0);
        }
    }
}