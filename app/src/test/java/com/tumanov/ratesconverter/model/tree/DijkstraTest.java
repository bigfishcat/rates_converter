package com.tumanov.ratesconverter.model.tree;

import com.tumanov.ratesconverter.model.Rate;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Created by tumanov on 3/25/2016.
 */
public class DijkstraTest {
    @Test
    public void testGetRateFrom() throws Exception {
        Dijkstra algorithm = new Dijkstra(new Graph(Arrays.asList(new Rate("RUB", "USD", 2), new Rate("USD", "CAN", 3), new Rate("CAN", "GBP", 5))), "GBP");
        assertEquals(30., algorithm.getRateFrom("RUB"), 0.1);

        algorithm = new Dijkstra(new Graph(
                Arrays.asList(
                        new Rate("AUD", "USD", 0.5f), new Rate("USD", "EUR", 0.5f), new Rate("EUR", "GBP", 0.5f),
                        new Rate("GBP", "EUR", 2), new Rate("EUR", "USD", 2), new Rate("USD", "AUD", 2)
                )
        ), "GBP");
        assertEquals(1., algorithm.getRateFrom("GBP"), 0.1);
        assertEquals(.5, algorithm.getRateFrom("EUR"), 0.01);
        assertEquals(.25, algorithm.getRateFrom("USD"), 0.001);
        assertEquals(.125, algorithm.getRateFrom("AUD"), 0.0001);
    }
}