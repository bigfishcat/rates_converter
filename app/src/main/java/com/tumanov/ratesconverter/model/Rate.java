package com.tumanov.ratesconverter.model;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Defined rate.
 */
public class Rate implements Serializable {
    private static final long serialVersionUID = 1476472293222776147L;

    public final String from;
    public final String to;
    public final float rate;

    public Rate(@NonNull String from, @NonNull String to, float rate) {
        this.from = from;
        this.to = to;
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rate)) return false;

        Rate rate1 = (Rate) o;

        if (Float.compare(rate1.rate, rate) != 0) return false;
        if (!from.equals(rate1.from)) return false;
        return to.equals(rate1.to);

    }

    @Override
    public int hashCode() {
        int result = from.hashCode();
        result = 31 * result + to.hashCode();
        result = 31 * result + (rate != +0.0f ? Float.floatToIntBits(rate) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Rate{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", rate=" + rate +
                '}';
    }

    public static List<String> asStringList(@NonNull Collection<Rate> rates) {
        List<String> strings = new ArrayList<>(rates.size());
        for (Rate r : rates) {
            strings.add(String.format("%s|%s|%f", r.from, r.to, r.rate));
        }
        return strings;
    }

    public static List<Rate> fromStrings(@NonNull Collection<String> strings) {
        List<Rate> rates = new ArrayList<>(strings.size());

        for (String s : strings) {
            String[] parts = s.split("|");
            if (parts.length != 3)
                continue;
            try {
                rates.add(new Rate(parts[0], parts[1], Float.parseFloat(parts[2])));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        return rates;
    }
}
