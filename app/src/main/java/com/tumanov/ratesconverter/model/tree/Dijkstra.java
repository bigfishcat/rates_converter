package com.tumanov.ratesconverter.model.tree;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/**
 * Realization of Dijkstra's algorithm.
 */
public class Dijkstra {
    private final Graph graph;
    private final String defCurrency;
    private final Map<String, String> predecessors = new HashMap<>();
    private final Map<String, Integer> distances = new HashMap<>();
    private final Queue<Vertex> availableVertices = new LinkedList<>();
    private final Set<Vertex> visitedVertices = new HashSet<>();

    public Dijkstra(Graph graph, String defCurrency){
        this.graph = graph;
        this.defCurrency = defCurrency;

        Set<String> vertexKeys = graph.vertices.keySet();
        if(!vertexKeys.contains(defCurrency))
            throw new IllegalArgumentException("The graph must contain the initial vertex.");

        for(String key: vertexKeys){
            predecessors.put(key, null);
            distances.put(key, Integer.MAX_VALUE);
        }
        distances.put(defCurrency, 0);

        Vertex initialVertex = graph.vertices.get(defCurrency);
        List<Edge> initialVertexNeighbors = initialVertex.neighborhood;
        for(Edge e : initialVertexNeighbors){
            Vertex other = e.getNeighbor(initialVertex);
            predecessors.put(other.currency, defCurrency);
            distances.put(other.currency, 1);
            availableVertices.add(other);
        }
        visitedVertices.add(initialVertex);

        processGraph();
    }

    private void processGraph(){
        while(availableVertices.size() > 0){
            Vertex next = availableVertices.poll();
            int distanceToNext = distances.get(next.currency);
            List<Edge> nextNeighbors = next.neighborhood;
            for(Edge e: nextNeighbors){
                Vertex other = e.getNeighbor(next);
                if(visitedVertices.contains(other)){
                    continue;
                }

                int currentWeight = distances.get(other.currency);
                int newWeight = distanceToNext + 1;

                if(newWeight < currentWeight){
                    predecessors.put(other.currency, next.currency);
                    distances.put(other.currency, newWeight);
                    availableVertices.remove(other);
                    availableVertices.add(other);
                }
            }
            visitedVertices.add(next);
        }
    }

    private List<Vertex> getPathTo(String currency){
        List<Vertex> path = new LinkedList<>();
        path.add(graph.vertices.get(currency));

        while(!currency.equals(defCurrency)){
            Vertex predecessor = graph.vertices.get(predecessors.get(currency));
            currency = predecessor.currency;
            path.add(0, predecessor);
        }
        return path;
    }

    public float getRateFrom(String currency) {
        List<Vertex> path = getPathTo(currency);
        float rate = 1.f;
        for (int i = 1; i < path.size(); i++) {
            Vertex left = path.get(i - 1);
            Vertex right = path.get(i);
            Edge edge = graph.getEdge(left, right);
            rate *= edge.getRate(right, left);
        }
        return rate;
    }
}
