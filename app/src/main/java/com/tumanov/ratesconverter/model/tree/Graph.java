package com.tumanov.ratesconverter.model.tree;

import android.support.annotation.NonNull;
import com.tumanov.ratesconverter.model.Rate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Graph of rates.
 */
public class Graph {
    final Map<String, Vertex> vertices = new HashMap<>();
    private final List<Edge> edges = new ArrayList<>();

    public Graph(List<Rate> rates) {
        for (Rate rate : rates) {
            if (rate.from.equals(rate.to))
                continue;

            Vertex left = addVertex(rate.from);
            Vertex right = addVertex(rate.to);

            addEdge(left, right).setRate(left, right, rate.rate);
        }
    }

    private Vertex addVertex(String currency) {
        if (vertices.containsKey(currency))
            return vertices.get(currency);

        Vertex v = new Vertex(currency);
        vertices.put(currency, v);
        return v;
    }

    private Edge addEdge(Vertex left, Vertex right) {
        Edge edge = new Edge(left, right);

        if (edges.contains(edge)) {
            edge = edges.get(edges.indexOf(edge));
        } else {
            edges.add(edge);
        }

        left.addNeighbor(edge);
        right.addNeighbor(edge);
        return edge;
    }

    Edge getEdge(@NonNull Vertex left, @NonNull Vertex right) {
        int index = edges.indexOf(new Edge(left, right));
        return index > -1 ? edges.get(index) : null;
    }
}
