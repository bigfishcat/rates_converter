package com.tumanov.ratesconverter.model.tree;

import android.support.annotation.NonNull;

/**
 * Edge for graph of rates.
 */
class Edge {
    private final Vertex left, right;
    private float ltrRate, rtlRate;

    Edge(@NonNull Vertex left, @NonNull Vertex right) {
        this.left = left;
        this.right = right;
    }

    void setRate(@NonNull Vertex left, @NonNull Vertex right, float rate) {
        if (this.left.equals(left) && this.right.equals(right))
            ltrRate = rate;
        if (this.right.equals(left) && this.left.equals(right))
            rtlRate = rate;
    }

    float getRate(@NonNull Vertex left, @NonNull Vertex right) {
        if (this.left.equals(left) && this.right.equals(right)) {
            return ltrRate <= 0.f && rtlRate > 0.f ? 1 / rtlRate : ltrRate;
        }
        if (this.right.equals(left) && this.left.equals(right)) {
            return rtlRate <= 0.f && ltrRate > 0.f ? 1 / ltrRate : rtlRate;
        }
        return 0.f;
    }

    Vertex getNeighbor(Vertex current){
        if(!(current.equals(left) || current.equals(right))){
            return null;
        }
        return (current.equals(left)) ? right : left;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Edge)) return false;

        Edge edge = (Edge) o;

        return left.equals(edge.left) && right.equals(edge.right) ||
               left.equals(edge.right) && right.equals(edge.left);
    }

    @Override
    public int hashCode() {
        return left.hashCode() + right.hashCode();
    }
}
