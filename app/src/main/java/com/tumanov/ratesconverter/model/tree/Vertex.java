package com.tumanov.ratesconverter.model.tree;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Vertex for graph of rates.
 */
class Vertex {
    final List<Edge> neighborhood = new ArrayList<>();
    final String currency;

    Vertex(@NonNull String currency) {
        this.currency = currency;
    }

    void addNeighbor(Edge edge){
        if(neighborhood.contains(edge)){
            neighborhood.remove(edge);
        }
        neighborhood.add(edge);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vertex)) return false;

        Vertex vertex = (Vertex) o;
        return currency.equals(vertex.currency);
    }

    @Override
    public int hashCode() {
        return currency.hashCode();
    }
}
