package com.tumanov.ratesconverter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

public class CurrenciesPagerAdapter extends FragmentPagerAdapter {
    private List<String> currencies;
    private CurrencyInputFragment.Listener listener;

    public CurrenciesPagerAdapter(FragmentManager fm, @NonNull List<String> currencies, CurrencyInputFragment.Listener listener) {
        super(fm);
        this.currencies = currencies;
        this.listener =  listener;
    }

    @Override
    public Fragment getItem(int position) {
        CurrencyInputFragment fragment = CurrencyInputFragment.newInstance(getCurrency(position));
        fragment.setListener(listener);
        return fragment;
    }

    public String getCurrency(int position) {
        return currencies.get(position);
    }

    @Override
    public int getCount() {
        return currencies.size();
    }
}
