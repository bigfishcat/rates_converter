package com.tumanov.ratesconverter.provider;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.tumanov.ratesconverter.model.Rate;
import com.tumanov.ratesconverter.model.tree.Dijkstra;
import com.tumanov.ratesconverter.model.tree.Graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Provide specific conversion rate between two currencies.
 */
public class ConversionsProvider {
    private static final String KEY = "rates_set";

    private final RatesProvider ratesProvider;
    private final Map<String, Dijkstra> algoCache = new HashMap<>();
    private Graph conversionGraph;
    private List<Rate> rates = Collections.emptyList();

    public ConversionsProvider(@NonNull RatesProvider ratesProvider) {
        this(ratesProvider, null);
    }

    public ConversionsProvider(RatesProvider ratesProvider, SharedPreferences prefs) {
        this.ratesProvider = ratesProvider;
        if (prefs != null)
            restore(prefs);
        init();
    }

    public float getConversionRate(String from, String to) {
        for (Rate rate: rates) {
            if (rate.from.equals(from) && rate.to.equals(to))
                return rate.rate;
        }

        if (rates.size() < 2)
            return Float.NaN;

        Dijkstra algo = algoCache.get(to);
        if (algo == null) {
            algo = new Dijkstra(conversionGraph, to);
            algoCache.put(to, algo);
        }
        return algo.getRateFrom(from);
    }

    /**
     * Get new conversion rates from ECB and build new Graph of conversions.
     * @return true if new rates was loaded
     */
    public boolean refresh() {
        rates = ratesProvider.getRates();
        if (!rates.isEmpty())
            init();
        return !rates.isEmpty();
    }

    public List<String> getAvailableCurrencies() {
        Set<String> currencies = new HashSet<>(rates.size());

        for (Rate rate : rates) {
            currencies.add(rate.from);
            currencies.add(rate.to);
        }

        return new ArrayList<>(currencies);
    }

    public void save(SharedPreferences prefs) {
        prefs.edit().putStringSet(KEY, new HashSet<>(Rate.asStringList(rates))).apply();
    }

    private void init() {
        conversionGraph = new Graph(rates);
        algoCache.clear();
    }

    private void restore(SharedPreferences prefs) {
        rates = Rate.fromStrings(prefs.getStringSet(KEY, Collections.<String>emptySet()));
    }
}
