package com.tumanov.ratesconverter.provider;

import android.support.annotation.NonNull;
import com.tumanov.ratesconverter.model.Rate;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * This provider download exchange rates XML from ECB and parse it as List of @Rate.
 */
public class RatesProvider {
    private static final String FROM = "EUR";
    private static final String LINK = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";

    public @NonNull List<Rate> getRates() {
        URL url;
        try {
            url = new URL(LINK);
            URLConnection urlConnection = url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            return parseRates(in);
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    List<Rate> parseRates(@NonNull InputStream input) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = createXmlParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(input, null);
            parser.nextTag();
            return readRates(parser);
        } finally {
            input.close();
        }
    }

    private List<Rate> readRates(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<Rate> rates = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, null, "gesmes:Envelope");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("Cube")) {
                rates.addAll(readCube(parser));
            } else {
                skip(parser);
            }
        }
        return rates;
    }

    private List<Rate> readCube(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, null, "Cube");
        List<Rate> rates = new LinkedList<>();
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("Cube")) {
                if (parser.getAttributeCount() == 2) {
                    rates.add(readRate(parser));
                } else {
                    rates.addAll(readCube(parser));
                }
            } else {
                skip(parser);
            }
        }
        return rates;
    }

    private Rate readRate(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "Cube");
        float rate = Float.parseFloat(parser.getAttributeValue(null, "rate"));
        String to = parser.getAttributeValue(null, "currency");
        parser.nextTag();
        parser.require(XmlPullParser.END_TAG, null, "Cube");
        return new Rate(FROM, to, rate);
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    private XmlPullParser createXmlParser() throws XmlPullParserException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        return factory.newPullParser();
    }
}
