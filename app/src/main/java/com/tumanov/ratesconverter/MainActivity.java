package com.tumanov.ratesconverter;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int FIRST = 0;
    private static final int SECOND = 1;

    private ExchangeRatesUpdateService.LocalBinder binder;
    private View placeholder;
    private View container;
    private CurrenciesPagerAdapter adapterFrom, adapterTo;
    private ViewPager pagerTo, pagerFrom;
    private CurrencyInputFragment fragmentFrom, fragmentTo;
    private FragmentCallback fragmentFromListener = new FragmentCallback(FIRST);
    private FragmentCallback fragmentToListener = new FragmentCallback(SECOND);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        placeholder = findViewById(R.id.placeholder);
        container = findViewById(R.id.container);
    }

    private void initConverter() {
        pagerFrom = setupViewPager(R.id.pager_from, R.id.pager_from_indicator, FIRST,
                adapterFrom = new CurrenciesPagerAdapter(getSupportFragmentManager(), usedCurrencies(), fragmentFromListener));
        pagerTo = setupViewPager(R.id.pager_to, R.id.pager_to_indicator, SECOND,
                adapterTo = new CurrenciesPagerAdapter(getSupportFragmentManager(), usedCurrencies(), fragmentToListener));
        container.setVisibility(View.VISIBLE);
        placeholder.setVisibility(View.GONE);
    }

    private ViewPager setupViewPager(int pagerId, int indicatorId, int which, CurrenciesPagerAdapter adapter) {
        ViewPager pager = (ViewPager) container.findViewById(pagerId);
        CirclePageIndicator indicator = (CirclePageIndicator) container.findViewById(indicatorId);
        pager.setAdapter(adapter);
        indicator.setViewPager(pager);
        indicator.setOnPageChangeListener(new OnPageChangeListener(which));
        return pager;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, ExchangeRatesUpdateService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(connection);
        if (binder != null)
            binder.setListener(null);
    }

    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            binder = (ExchangeRatesUpdateService.LocalBinder) service;
            binder.setListener(serviceListener);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
        }
    };

    private ExchangeRatesUpdateService.Listener serviceListener = new ExchangeRatesUpdateService.Listener() {
        @Override
        public void onRatesUpdated(List<String> availableCurrencies) {
            if (placeholder.getVisibility() == View.VISIBLE)
                initConverter();
            else if (getFragmentTo() != null && getFragmentTo().isActive())
                getFragmentTo().touch();
            else if (getFragmentFrom() != null)
                getFragmentFrom().touch();
        }

        @Override
        public void onConversionRateReceived(String from, String to, float rate) {
            if (getActiveFragment() == null)
                return;
            updateHint(from, to, rate);
            updateHint(to, from, 1.f / rate);

            if (TextUtils.equals(from, to)) {
                updateValue(to, getActiveFragment().getValue());
            } else {
                updateValue(to, rate * getActiveFragment().getValue());
            }
        }
    };

    private void updateValue(String to, float value) {
        LocalBroadcastManager.getInstance(this).sendBroadcastSync(
                new Intent(CurrencyInputFragment.UPDATE_VALUE_ACTION)
                        .putExtra(CurrencyInputFragment.TARGET_KEY, to)
                        .putExtra(CurrencyInputFragment.VALUE_KEY, value)
        );
    }

    private void updateHint(String from, String to, float rate) {
        LocalBroadcastManager.getInstance(this).sendBroadcastSync(
                new Intent(CurrencyInputFragment.UPDATE_RATE_HINT_ACTION)
                        .putExtra(CurrencyInputFragment.TARGET_KEY, from)
                        .putExtra(CurrencyInputFragment.RATE_HINT_KEY, getRateHint(from, to, rate))
        );
    }

    private String getRateHint(String from, String to, float rate) {
        return TextUtils.equals(from, to) ? null : String.format("1 %s = %.4f %s", from, rate, to);
    }

    private CurrencyInputFragment getActiveFragment() {
        return getFragmentTo().isActive() ? getFragmentTo() : getFragmentFrom();
    }

    private CurrencyInputFragment getFragmentFrom() {
        if (fragmentFrom == null) {
            fragmentFrom = getFragmentByTag(pagerFrom, R.id.pager_from);
        }
        return fragmentFrom;
    }

    private CurrencyInputFragment getFragmentTo() {
        if (fragmentTo == null) {
            fragmentTo = getFragmentByTag(pagerTo, R.id.pager_to);
        }
        return fragmentTo;
    }

    private CurrencyInputFragment getFragmentByTag(ViewPager pager, int id) {
        return (CurrencyInputFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + id + ":" + pager.getCurrentItem());
    }

    private String getPairCurrency(int which) {
        switch (which) {
            case FIRST:
                return adapterTo.getCurrency(pagerTo.getCurrentItem());
            case SECOND:
                return adapterFrom.getCurrency(pagerFrom.getCurrentItem());
            default:
                throw new IllegalStateException("@which should be on of [FIRST, SECOND]");
        }
    }

    private List<String> usedCurrencies() {
        return Arrays.asList("EUR", "USD", "GBP");
    }

    private class OnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {
        private int which;

        private OnPageChangeListener(int which) {
            this.which = which;
        }

        @Override
        public void onPageSelected(int position) {
            getSupportFragmentManager().executePendingTransactions();
            switch (which) {
                case FIRST:
                    fragmentFrom = null;
                    getFragmentTo().touch();
                    break;
                case SECOND:
                    fragmentTo = null;
                    getFragmentFrom().touch();
                    break;
                default:
                    throw new IllegalStateException("@which should be on of [FIRST, SECOND]");
            }

        }
    }

    private class FragmentCallback implements CurrencyInputFragment.Listener {
        private int which;

        private FragmentCallback(int which) {
            this.which = which;
        }

        @Override
        public void requestRate(String from) {
            String to = getPairCurrency(which);
            if (TextUtils.equals(from, to))
                serviceListener.onConversionRateReceived(from, to, 1.0f);
            else
                binder.requestConversionRate(from, to);
        }
    }
}
