package com.tumanov.ratesconverter;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.tumanov.ratesconverter.provider.ConversionsProvider;
import com.tumanov.ratesconverter.provider.RatesProvider;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class ExchangeRatesUpdateService extends Service {

    private static final int REFRESH_RATES = 0;
    private static final int RATES_REFRESHED = 1;
    private static final int GET_CONVERSION_RATE = 2;
    private static final int CONVERSION_RATE_RECEIVED = 3;
    private static final String AVAILABLE_CURRENCIES_KEY = "AVAILABLE_CURRENCIES_KEY";
    private static final String FROM_CURRENCY_KEY = "FROM_CURRENCY_KEY";
    private static final String TO_CURRENCY_KEY = "TO_CURRENCY_KEY";
    private static final String RATE_KEY = "RATE_KEY";
    private static final long REFRESH_DELAY = 30000L;

    class LocalBinder extends Binder {
        void setListener(Listener listener) {
            producerHandler.consumerHandler.setListener(listener);
            if (listener != null)
                producerHandler.obtainMessage(REFRESH_RATES).sendToTarget();
        }

        void requestConversionRate(String from, String to) {
            if (producerHandler != null) {
                Message msg = producerHandler.obtainMessage(GET_CONVERSION_RATE);
                msg.getData().putString(FROM_CURRENCY_KEY, from);
                msg.getData().putString(TO_CURRENCY_KEY, to);
                msg.sendToTarget();
            }
        }
    }

    interface Listener {
        void onRatesUpdated(List<String> availableCurrencies);
        void onConversionRateReceived(String from, String to, float rate);
    }

    private HandlerThread workerThread;
    private ProducerHandler producerHandler;
    private final IBinder binder = new LocalBinder();

    @Override
    public void onCreate() {
        super.onCreate();
        workerThread = new HandlerThread("rates_updater");
        workerThread.start();
        producerHandler = new ProducerHandler(workerThread.getLooper(), this);
        producerHandler.consumerHandler = new ConsumerHandler();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (producerHandler != null) {
            producerHandler.onDestroy();
            producerHandler = null;
        }
        if (workerThread != null) {
            workerThread.quit();
            workerThread = null;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    private static boolean isNetworkAvailable(@NonNull Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }

    private static class ConsumerHandler extends Handler {
        private WeakReference<Listener> listenerRef = new WeakReference<>(null);

        synchronized
        private void setListener(Listener listener) {
            listenerRef = new WeakReference<>(listener);
        }

        synchronized
        private Listener getListener() {
            return listenerRef.get();
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Listener listener = getListener();
            if (listener != null) {
                processMessage(msg, listener);
            }
        }

        private void processMessage(@NonNull Message msg, @NonNull Listener listener) {
            Bundle data = msg.getData();
            switch (msg.what) {
                case RATES_REFRESHED:
                    listener.onRatesUpdated(data.getStringArrayList(AVAILABLE_CURRENCIES_KEY));
                    break;
                case CONVERSION_RATE_RECEIVED:
                    listener.onConversionRateReceived(
                            data.getString(FROM_CURRENCY_KEY),
                            data.getString(TO_CURRENCY_KEY),
                            data.getFloat(RATE_KEY)
                    );
                    break;
            }
        }
    }

    private static class ProducerHandler extends Handler {
        private ConsumerHandler consumerHandler;
        private ConversionsProvider provider;
        private SharedPreferences preferences;
        private WeakReference<Context> ctxRef;
        private BroadcastReceiver receiver;

        private void onDestroy() {
            Context ctx = ctxRef.get();
            if (ctx != null && receiver != null) {
                ctx.unregisterReceiver(receiver);
            }
        }

        private void waitNetwork(Context ctx) {
            if (receiver != null) {
                ctx.registerReceiver(new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (isNetworkAvailable(context)) {
                            context.unregisterReceiver(this);
                            refreshRates();
                            receiver = null;
                        }
                    }
                }, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            }
        }

        public ProducerHandler(@NonNull Looper looper, Context context) {
            super(looper);
            preferences = PreferenceManager.getDefaultSharedPreferences(context);
            provider = new ConversionsProvider(new RatesProvider(), preferences);
            ctxRef = new WeakReference<>(context);
        }

        private void refreshRates() {
            long delay = System.currentTimeMillis();
            provider.refresh();
            if (consumerHandler != null) {
                Message m =  consumerHandler.obtainMessage(RATES_REFRESHED);
                m.getData().putStringArrayList(AVAILABLE_CURRENCIES_KEY, new ArrayList<>(provider.getAvailableCurrencies()));
                m.sendToTarget();
            }

            delay -= System.currentTimeMillis();
            delay += REFRESH_DELAY;
            sendMessageDelayed(obtainMessage(REFRESH_RATES), delay);
            provider.save(preferences);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case REFRESH_RATES:
                    Context ctx = ctxRef.get();
                    if (ctx != null) {
                        if (isNetworkAvailable(ctx))
                            refreshRates();
                        else
                            waitNetwork(ctx);
                    }
                    break;
                case GET_CONVERSION_RATE:
                    if (provider != null && consumerHandler != null) {
                        String from = msg.getData().getString(FROM_CURRENCY_KEY);
                        String to = msg.getData().getString(TO_CURRENCY_KEY);
                        float rate = provider.getConversionRate(from, to);
                        Message m =  consumerHandler.obtainMessage(CONVERSION_RATE_RECEIVED);
                        m.getData().putString(FROM_CURRENCY_KEY, from);
                        m.getData().putString(TO_CURRENCY_KEY, to);
                        m.getData().putFloat(RATE_KEY, rate);
                        m.sendToTarget();
                    }
                    break;
            }
        }
    }
}
