package com.tumanov.ratesconverter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class CurrencyInputFragment extends Fragment implements View.OnFocusChangeListener, TextWatcher {
    public static final String UPDATE_VALUE_ACTION = CurrencyInputFragment.class.getName() + "/UPDATE_VALUE_ACTION";
    public static final String UPDATE_RATE_HINT_ACTION = CurrencyInputFragment.class.getName() + "/UPDATE_RATE_HINT_ACTION";
    public static final String RATE_HINT_KEY = "RATE_HINT_KEY";
    public static final String VALUE_KEY = "VALUE_KEY";
    public static final String TARGET_KEY = "TARGET_KEY";

    private static final String CURRENCY_KEY = "currency";

    public void setListener(Listener callback) {
        this.callback = callback;
    }

    public interface Listener {
        void requestRate(String from);
    }

    public static CurrencyInputFragment newInstance(String currency) {
        CurrencyInputFragment instance = new CurrencyInputFragment();
        Bundle args = new Bundle();
        args.putString(CurrencyInputFragment.CURRENCY_KEY, currency);
        instance.setArguments(args);
        return instance;
    }

    private String currency;
    private EditText input;
    private TextView rateHint;
    private Listener callback;
    private DecimalFormat formatter = new DecimalFormat("#.####");

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.input_currency_layout, container, false);
        currency = getArguments().getString(CURRENCY_KEY);
        ((TextView)root.findViewById(R.id.currency_title)).setText(currency);
        input = (EditText) root.findViewById(R.id.input);
        rateHint = (TextView) root.findViewById(R.id.rate_hint);
        input.setOnFocusChangeListener(this);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(UPDATE_RATE_HINT_ACTION);
        filter.addAction(UPDATE_VALUE_ACTION);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(receiver);
    }

    @Override
    public void onFocusChange(View view, boolean focus) {
        if (focus) {
            input.addTextChangedListener(this);
            setValue(getValue());
            if (callback != null && !TextUtils.isEmpty(input.getText()))
                callback.requestRate(currency);
            input.setSelection(input.getText().length());
        } else {
            input.removeTextChangedListener(this);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (isActive() && callback != null && !TextUtils.isEmpty(charSequence))
            callback.requestRate(currency);
    }

    @Override
    public void afterTextChanged(Editable editable) {}

    public boolean isActive() {
        return input != null && input.hasFocus();
    }

    public void touch() {
        if (callback != null && !TextUtils.isEmpty(input.getText()))
            callback.requestRate(currency);
    }

    public void setValue(float value) {
        if (input != null)
            input.setText(Float.isNaN(value) ? "" : formatter.format(value));
    }

    public float getValue() {
        try {
            return input != null && !TextUtils.isEmpty(input.getText()) ? formatter.parse(input.getText().toString()).floatValue() : 0.f;
        } catch (ParseException e) {
            return Float.NaN;
        }
    }

    public void updateRateHint(String hint) {
        if (TextUtils.isEmpty(hint)) {
            rateHint.setVisibility(View.INVISIBLE);
        } else {
            rateHint.setText(hint);
            rateHint.setVisibility(View.VISIBLE);
        }
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String target = intent.getStringExtra(TARGET_KEY);
            if (TextUtils.equals(target, currency)) {
                if (UPDATE_RATE_HINT_ACTION.equals(intent.getAction())) {
                    updateRateHint(intent.getStringExtra(RATE_HINT_KEY));
                } else if (UPDATE_VALUE_ACTION.equals(intent.getAction()) && !isActive()) {
                    setValue(intent.getFloatExtra(VALUE_KEY, Float.NaN));
                }
            }
        }
    };
}
