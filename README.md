Download the Revolut app.

Navigate to the exchange screen.

Implement this screen in your own custom app using the fx rates from here:

* Docs: http://www.ecb.int/stats/exchange/eurofxref/html/index.en.html#dev
* Request: http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml

## Explicit Requirements:

* Your app should poll the openexchangerates endpoint every 30 seconds to get the latest rates for GBP, EUR and USD. (The API provides close of day FX rates. We expect you to request the new rate every 30s. We do not expect the rate to change every 30s)

* Focus more on code design rather than UI, but if you have time at the end of the task, please try and mimic the Revolut UI to a certain extent

## Implicit Requirements:

* The code produced is expected to be of very good quality


Please put your work on bitbucket.